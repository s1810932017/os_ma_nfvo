from django.urls import path, include
from rest_framework.routers import DefaultRouter
from VnfPackageSubscription import views

router = DefaultRouter()
router.register(r'subscriptions', views.VNFPackageSubscriptionViewSet)

urlpatterns = [
    path('vnfpkgm/v1/', include(router.urls)),
]


