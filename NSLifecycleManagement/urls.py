from django.urls import path, include
from rest_framework.routers import DefaultRouter
from NSLifecycleManagement import views

router = DefaultRouter()
router.register(r'ns_instances', views.NSLifecycleManagementViewSet)

urlpatterns = [
    path('nslcm/v1/', include(router.urls)),
]
