from django.urls import path, include
from rest_framework.routers import DefaultRouter
from NSLifecycleSubscriptions import views

router = DefaultRouter()
router.register(r'subscriptions', views.NSLifecycleSubscriptionsViewSet)

urlpatterns = [
    path('nslcm/v1/', include(router.urls)),
]
