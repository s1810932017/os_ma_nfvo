from django.urls import path
from VIMManagement import views

urlpatterns = [
    path('vimm/v1/kubernetes', views.kubernetes_resource),
]
