import requests
import json

import os_ma_nfvo.settings as setting
from VIMManagement.utils.kubernetes_api import KubernetesApi


class RetryPolicyClient:
    def __init__(self, *args, **kwargs):

        self.url = setting.SERVICE_MESH_IP + 'istio/retry_policy/'
        self.headers = {'Content-Type': 'application/json'}

        self.retry_policy_json = dict()
        self.retry_policy_json['name'] = kwargs['retry_policy_name']
        self.retry_policy_json['host'] = kwargs['retry_policy_host']
        attempts=kwargs['retry_policy_attempts']
        perTryTimeout=kwargs['retry_policy_perTryTimeout']
        self.retry_policy_json['attempts'] = int(attempts)
        self.retry_policy_json['perTryTimeout'] = int(perTryTimeout)

    def delete_resource(self):
        self.response = requests.delete(self.url, headers=self.headers, data=json.dumps(self.retry_policy_json),
                                        verify=False)

    def create_resource(self):
        self.response = requests.post(self.url, headers=self.headers, data=json.dumps(self.retry_policy_json),
                                      verify=False)
