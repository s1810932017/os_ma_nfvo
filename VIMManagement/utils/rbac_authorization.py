from VIMManagement.utils.kubernetes_api import KubernetesApi


class RBACClient(KubernetesApi):
    def __init__(self, *args, **kwargs):
        kwargs['instance_name'] = 'admin-user'
        super().__init__(*args, **kwargs)

    def read_resource(self, **kwargs):
        return self.rbac_authorization_v1.read_cluster_role_binding(name=self.instance_name)

    def create_resource(self, **kwargs):
        self.rbac_authorization_v1.create_cluster_role_binding(body=self.resource)

    def patch_resource(self, **kwargs):
        self.rbac_authorization_v1.patch_cluster_role_binding(
            name=self.instance_name, body=self.resource)

    def delete_resource(self, **kwargs):
        self.rbac_authorization_v1.delete_cluster_role_binding(
            name=self.instance_name, body=self.delete_options)

    def instance_specific_resource(self, **kwargs):
        role_ref = self.kubernetes_client.V1RoleRef(
            api_group='rbac.authorization.k8s.io', kind='ClusterRole', name=self.instance_name)
        subjects = list()
        subjects.append(self.kubernetes_client.V1Subject(
            kind='ServiceAccount', name='default', namespace='default'))
        cluster_role_binding = self.kubernetes_client.V1ClusterRoleBinding(
            api_version="rbac.authorization.k8s.io/v1", kind="ClusterRoleBinding",
            role_ref=role_ref, subjects=subjects)
        cluster_role_binding.metadata = self.kubernetes_client.V1ObjectMeta(name=self.instance_name)
        return cluster_role_binding
