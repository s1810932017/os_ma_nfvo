from VIMManagement.utils.kubernetes_api import KubernetesApi


class ServiceClient(KubernetesApi):
    def __init__(self, *args, **kwargs):
        self.service_type = kwargs['service_type'] if 'service_type' in kwargs else None
        self.ports = kwargs['ports'] if 'ports' in kwargs else None
        self.protocol = kwargs['protocol'] if 'protocol' in kwargs else None
        super().__init__(*args, **kwargs)

    def read_resource(self, **kwargs):
        return self.core_v1.read_namespaced_service(self.instance_name, self.namespace)

    def create_resource(self, **kwargs):
        self.core_v1.create_namespaced_service(self.namespace, self.resource)

    def patch_resource(self, **kwargs):
        self.core_v1.patch_namespaced_service(self.instance_name, self.namespace, self.resource)

    def delete_resource(self, **kwargs):
        self.core_v1.delete_namespaced_service(
            name=self.instance_name, namespace=self.namespace, body=self.delete_options)

    def instance_specific_resource(self, **kwargs):
        service = self.kubernetes_client.V1Service(api_version="v1", kind="Service")
        service.metadata = self.kubernetes_client.V1ObjectMeta(name=self.instance_name)
        service.spec = self.kubernetes_client.V1ServiceSpec(
            cluster_ip='None', selector={'app': self.instance_name}, ports=self._get_service_port(), type=self.service_type)
        return service

    def _get_service_port(self):
        if self.protocol is None:
            protocol = 'TCP'
            return [self._create_service_port(protocol, port) for port in self.ports]
        else:
            i = 0
            service_port = list()
            for protocol in self.protocol:
                service_port.append(self._create_service_port(protocol, self.ports[i]))
                i = i + 1
            return service_port

    def _create_service_port(self, protocol, port):
        return self.kubernetes_client.V1ServicePort(
            name='{}{}'.format(self.instance_name[-10:], port), port=int(port), protocol=protocol)
