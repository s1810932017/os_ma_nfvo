import requests
import json

import os_ma_nfvo.settings as setting
from VIMManagement.utils.kubernetes_api import KubernetesApi


class CircuitBreakingClient():
	def __init__(self, *args, **kwargs):
		# print(kwargs['circuit_breaking_name'])
		# print(kwargs['circuit_breaking_host'])
		# print(kwargs['circuit_breaking_maxConnections'])
		# print(kwargs['circuit_breaking_maxRequestsPerConnection'])

		self.url = setting.SERVICE_MESH_IP + 'istio/circuit_breaking/'
		self.headers = {'Content-Type': 'application/json'}

		self.circuit_breaking_json = dict()
		self.circuit_breaking_json['name'] = kwargs['circuit_breaking_name']
		self.circuit_breaking_json['host'] = kwargs['circuit_breaking_host']
		self.circuit_breaking_json['maxConnections'] = kwargs['circuit_breaking_maxConnections']
		self.circuit_breaking_json['maxRequestsPerConnection'] = kwargs['circuit_breaking_maxRequestsPerConnection']

	def delete_resource(self):
		self.response = requests.delete(self.url, headers=self.headers, data=json.dumps(self.circuit_breaking_json),
								 verify=False)
	def create_resource(self):
		self.response = requests.post(self.url, headers=self.headers, data=json.dumps(self.circuit_breaking_json),
								 verify=False)