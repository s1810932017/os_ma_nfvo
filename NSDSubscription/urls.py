from django.urls import path, include
from rest_framework.routers import DefaultRouter
from NSDSubscription import views

router = DefaultRouter()
router.register(r'subscriptions', views.VNFPackageSubscriptionViewSet)

urlpatterns = [
    path('nsd/v1/', include(router.urls)),
]
