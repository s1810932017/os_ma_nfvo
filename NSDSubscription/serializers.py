from rest_framework import serializers

from utils.format_tools import transform_representation
from .models import *


class NsdmNotificationsFilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = NsdmNotificationsFilter
        fields = ('notificationTypes', 'nsdInfoId', 'nsdId', 'nsdName', 'nsdVersion', 'nsdDesigner',
                  'nsdInvariantId', 'vnfPkgIds', 'pnfdInfoIds', 'nestedNsdInfoIds', 'nsdOnboardingState',
                  'nsdOperationalState', 'nsdUsageState', 'pnfdId', 'pnfdName', 'pnfdVersion', 'pnfdProvider',
                  'pnfdInvariantId', 'pnfdOnboardingState', 'pnfdUsageState')

    def to_representation(self, instance):
        return transform_representation(super().to_representation(instance))


class NsdmSubscriptionLinkSerializer(serializers.ModelSerializer):
    self = serializers.CharField(source='link_self')

    class Meta:
        model = NsdmSubscriptionLink
        fields = ('self',)


class NsdmSubscriptionSerializer(serializers.ModelSerializer):
    _links = NsdmSubscriptionLinkSerializer(required=False, source='NsdmSubscription_links')
    filter = NsdmNotificationsFilterSerializer(required=False, source='NsdmSubscription_filter')

    class Meta:
        model = NsdmSubscription
        fields = '__all__'

    def create(self, validated_data):
        filter_value = validated_data.pop('NsdmSubscription_filter', None)
        link_value = validated_data.pop('NsdmSubscription_links')
        nsdm = NsdmSubscription.objects.create(**validated_data)
        NsdmSubscriptionLink.objects.create(_links=nsdm,
                                            **{'link_self': link_value['link_self'] + str(nsdm.id)})
        if filter_value:
            NsdmNotificationsFilter.objects.create(filter=nsdm,
                                                   **filter_value)
        return nsdm
