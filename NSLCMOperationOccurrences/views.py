from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action

from NSLCMOperationOccurrences.serializers import *


class NSLCMOperationOccurrencesViewSet(viewsets.ModelViewSet):
    queryset = NsLcmOpOcc.objects.all()
    serializer_class = NsLcmOpOccSerializer


@action(detail=True, methods=['POST'], url_path='retry')
def retry(self, request, **kwargs):
    ns_lcm_operation_occurrences = self.get_object()
    return Response(status=status.HTTP_202_ACCEPTED,
                    headers={'Location': ns_lcm_operation_occurrences.NsInstance_links.link_self})


@action(detail=True, methods=['POST'], url_path='rollback')
def rollback(self, request, **kwargs):
    ns_lcm_operation_occurrences = self.get_object()
    return Response(status=status.HTTP_202_ACCEPTED,
                    headers={'Location': ns_lcm_operation_occurrences.NsInstance_links.link_self})


@action(detail=True, methods=['POST'], url_path='continue')
def continue_ns_lcm(self, request, **kwargs):
    ns_lcm_operation_occurrences = self.get_object()
    return Response(status=status.HTTP_202_ACCEPTED,
                    headers={'Location': ns_lcm_operation_occurrences.NsInstance_links.link_self})


@action(detail=True, methods=['POST'], url_path='fail')
def fail(self, request, **kwargs):
    ns_lcm_operation_occurrences = self.get_object()
    return Response(status=status.HTTP_202_ACCEPTED,
                    headers={'Location': ns_lcm_operation_occurrences.NsInstance_links.link_self})


@action(detail=True, methods=['POST'], url_path='cancel')
def cancel(self, request, **kwargs):
    ns_lcm_operation_occurrences = self.get_object()
    return Response(status=status.HTTP_202_ACCEPTED,
                    headers={'Location': ns_lcm_operation_occurrences.NsInstance_links.link_self})
