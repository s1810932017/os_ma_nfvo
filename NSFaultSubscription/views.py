import json
import uuid

from rest_framework import viewsets, status
from rest_framework.exceptions import APIException

from NSFaultSubscription.models import FmSubscription
from NSFaultSubscription.serializers import FmSubscriptionSerializer
from NSLifecycleManagement.models import NsInstance


class NSFaultSubscriptionViewSet(viewsets.ModelViewSet):
    queryset = FmSubscription.objects.all()
    serializer_class = FmSubscriptionSerializer

    def create(self, request, **kwargs):
        if 'filter' not in request.data or 'callbackUri' not in request.data:
            raise APIException(detail='filter or callbackUri is not exists',
                               code=status.HTTP_409_CONFLICT)

        ns_instance_subscription_filter = request.data['filter'].get('nsInstanceSubscriptionFilter', None)
        if not ns_instance_subscription_filter:
            raise APIException(detail='nsInstanceSubscriptionFilter is not exists',
                               code=status.HTTP_409_CONFLICT)

        if not isinstance(ns_instance_subscription_filter, dict):
            raise APIException(detail='nsInstanceSubscriptionFilter needs dict type',
                               code=status.HTTP_409_CONFLICT)

        ns_instance_id = ns_instance_subscription_filter.get('nsInstanceIds', None)
        if not ns_instance_id:
            raise APIException(detail='nsInstanceIds is not exists',
                               code=status.HTTP_409_CONFLICT)

        if not isinstance(ns_instance_id, list):
            raise APIException(detail='nsInstanceIds needs list type',
                               code=status.HTTP_409_CONFLICT)

        for ns_id in ns_instance_id:
            ns_instance = NsInstance.objects.filter(id=uuid.UUID(ns_id)).last()
            if not ns_instance:
                raise APIException(detail='Network Service Instance not found',
                                   code=status.HTTP_404_NOT_FOUND)

        request.data['filter']['nsInstanceSubscriptionFilter']['nsInstanceIds'] = json.dumps(ns_instance_id)
        request.data['_links'] = {'self': request.build_absolute_uri()}
        return super().create(request)

    def get_success_headers(self, data):
        return {'Location': data['_links']['self']}

    def update(self, request, *args, **kwargs):
        raise APIException(detail='Method Not Allowed',
                           code=status.HTTP_405_METHOD_NOT_ALLOWED)

    def retrieve(self, request, *args, **kwargs):
        """
            Read an individual subscription resource.

            The GET method retrieves information about a subscription by reading an individual subscription resource. \
            This method shall support the URI query parameters, request and response data structures, \
            and response codes, as specified in the Tables 6.4.17.3.2-1 and 6.4.17.3.2-2
        """
        return super().retrieve(request)

    def list(self, request, *args, **kwargs):
        """
            Query multiple subscriptions.

            Query Subscription Information. \
            The GET method queries the list of active subscriptions of the functional block that invokes the method. \
            It can be used e.g. for resynchronization after error situations.
        """
        return super().list(request)

    def destroy(self, request, *args, **kwargs):
        """
            Terminate a subscription.

            The DELETE method terminates an individual subscription. \
            This method shall support the URI query parameters, request and response data structures, \
            and response codes, as specified in the Tables 6.4.17.3.5-1 and 6.4.17.3.5-2.
        """
        return super().destroy(request)
