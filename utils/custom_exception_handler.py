from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None:
        response.data['status'] = response.status_code \
            if 'error' == response.data['detail'].code else response.data['detail'].code

    return response
