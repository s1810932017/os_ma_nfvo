from abc import abstractmethod

from NSDSubscription.models import NsdmSubscription
from NSFaultSubscription.models import FmSubscription
from NSLifecycleSubscriptions.models import LccnSubscription
from VnfPackageSubscription.models import PkgmSubscription
from utils.base_request import BaseRequest


class NotificationManagement(object):
    Subscription_Type = (VNFPackage, NSDescriptor, NSInstance, FaultAlarm) = (
        'vnf_pkg', 'ns_descriptor', 'ns_instance', 'fault_alarm')

    def __init__(self, subscription_type):
        self.subscription_type = subscription_type
        self.header = None
        self.response = None

    def _set_header(self, header: dict):
        self.header = header

    def _initialization_response(self, response: dict):
        self.response = response

    def notify(self, subscription_id, notify_massage: str):
        print(notify_massage,'notify_massage')
        print(subscription_id, type(subscription_id), 'subscription_id', self.subscription_type)
        subscription = self._check_subscription_exist(subscription_id)
        print(subscription, 'subscriptionsubscription')
        if subscription:
            self._process_data(notify_massage)
            print(subscription.callbackUri,'subscription.callbackUri')
            a = BaseRequest(subscription.callbackUri).post(uri='', data=self.response, headers=self.header)
            print(a.content)

    def _check_subscription_exist(self, subscription_id):
        return {
            self.VNFPackage: PkgmSubscription.objects.filter(
                PkgmSubscription_filter__vnfPkgId__contains=subscription_id).last(),
            self.NSDescriptor: NsdmSubscription.objects.filter(
                NsdmSubscription_filter__nsdInfoId__contains=subscription_id).last(),
            self.NSInstance: LccnSubscription.objects.filter(
                filter__nsInstanceSubscriptionFilter__nsInstanceIds__contains=subscription_id).last(),
            self.FaultAlarm: FmSubscription.objects.filter(
                fm_subscription_fk_fm_subscriptions_filter__fm_subscription_filter_fk_ns_instance_subscription_filter__nsInstanceIds=subscription_id).last()
        }.get(self.subscription_type, None)

    @abstractmethod
    def _process_data(self, data):
        pass
